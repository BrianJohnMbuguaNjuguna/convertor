package com.example.convertor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView milimeters, inches;
    Button convert, exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        milimeters = findViewById(R.id.milimeters);
        inches = findViewById(R.id.inches);
        convert = findViewById(R.id.convert);
        exit = findViewById(R.id.exit);

        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double mm = Double.parseDouble(milimeters.getText().toString());
                double i = mm/25.4;

                inches.setText(i+"");
            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.finish();
                System.exit(0);
            }
        });
    }
}
